<?php
    
class Furniture extends Product{

   
    function setAttribute($dimensions){
        $this->attribute = "Dimensions: ".$dimensions->height."x".$dimensions->width."x".$dimensions->length;
    }

    function getAttribute(){
        $sku = $this->getSKU();

        $db = new DB();
        $conn = $db->db();

        $sql = "SELECT attribute FROM Products WHERE SKU = ?";
        $stmt = $conn->prepare($sql);
        $stmt->bind_param("s", $sku);   
        $stmt->execute();
        $stmt->bind_result($this->attribute);
        $stmt->fetch();

        $conn->close();

        return $this->attribute;
    }

    function displayForm(){
        return  "<div id='specForm'><br><br>
        Height(cm): <input  id='height' type='text' name='attribute' value=''>
        <span class='error'>* </span>
        <br><br>
        Width(cm): <input  id='width' type='text' name='attribute1' value=''>
        <span class='error'>*</span>
        <br><br>
        Length(cm): <input  id='length' type='text' name='attribute2' value=''>
        <span class='error'>* </span>
        <p>Please provide dimensions in HxWxL format.</p>
        <br><br></div>

        <script>
        function testSpecForm(){
            var height = document.getElementById('height').value; 
            var width = document.getElementById('width').value; 
            var length = document.getElementById('length').value; 
            var numbers = /^[0-9']+$/;

            var dimensionsError = '';
            if(height){
                if(!height.match(numbers)){
                    dimensionsError += 'Height: Please insert numbers only!<br><br>';
                }
            }
            else{
                dimensionsError += 'Height is required!<br><br>';
            }

            if(width){
                if(!width.match(numbers)){
                    dimensionsError += 'Width: Please insert numbers only!<br><br>';
                }
            }
            else{
                dimensionsError += 'Width is required!<br><br>';
            }

            if(length){
                if(!length.match(numbers)){
                    dimensionsError += 'Length: Please insert numbers only!<br><br>';
                }
            }
            else{
                dimensionsError += 'Length is required<br><br>';
            }

            document.getElementById('error_attribute').innerHTML = dimensionsError;

            if(!dimensionsError){
                formData.attribute = {
                    height : height,
                    width : width,
                    length: length};  
                return true;
            }
            else{
                return false;
            }
            
        }
        </script>";
     }

    function jsonSerialize(){
        return [
            'SKU' => $this->getSKU(),
            'name' => $this->getName(),
            'price' => $this->getPrice(),
            'attribute' => $this->getAttribute()
        ];
    }
}

?>