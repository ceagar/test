<?php
class DVD extends Product{

    function setAttribute($size){
        $this->attribute = "Size: ".$size."MB";
    }
    function getAttribute(){
        $sku = $this->getSKU();

        $db = new DB();
        $conn = $db->db();

        $sql = "SELECT attribute FROM Products WHERE SKU = ?";
        $stmt = $conn->prepare($sql);
        $stmt->bind_param("s", $sku);   
        $stmt->execute();
        $stmt->bind_result($this->attribute);
        $stmt->fetch();

        $conn->close();
        return $this->attribute;
    }

    function displayForm(){
    echo "<div id='specForm'><br><br>
    Size(MB): <input  id='size' type='text' name='attribute' value=''>
    <span class='error'> *</span>
    <p>Please provide disc size in MB.</p>
    <br><br></div>

    <script>
    function testSpecForm(){
        var size = document.getElementById('size').value; 
        var numbers = /^[0-9']+$/;

        var sizeError = '';
        if(size){
            if(!size.match(numbers)){
                sizeError = 'Size: Please insert numbers only!';
            }
        }
        else{
            sizeError = 'Size is required';
        }

        document.getElementById('error_attribute').innerHTML = sizeError;

        if(!sizeError){
            formData.attribute = size;  
            return true;
          }
          else{
            return false;
          }
        
    }
    </script>";
            
    }

    function jsonSerialize(){
        return [
            'SKU' => $this->getSKU(),
            'name' => $this->getName(),
            'price' => $this->getPrice(),
            'attribute' => $this->getAttribute()
        ];
    }
}

?>