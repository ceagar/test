<?php
require "DB.php";

abstract class Product implements  JsonSerializable{
    private $name;
    private $price;
    private $sku;
    private $category;
    protected $attribute;

    function setName($name){
        $this->name = $name;
    }

    function getName(){
        $db = new DB();
        $conn = $db->db();

        $sql = "SELECT name FROM Products WHERE SKU = ?";
        $stmt = $conn->prepare($sql);
        $stmt->bind_param("s", $this->sku);   
        $stmt->execute();
        $stmt->bind_result($this->name);
        $stmt->fetch();

        $conn->close();

        return $this->name;
    }

    function setPrice($price){
        $this->price = $price." $";
    }

    function getPrice(){
        $db = new DB();
        $conn = $db->db();

        $sql = "SELECT price FROM Products WHERE SKU = ?";
        $stmt = $conn->prepare($sql);
        $stmt->bind_param("s", $this->sku);   
        $stmt->execute();
        $stmt->bind_result($this->price);
        $stmt->fetch();

        $conn->close();
        return $this->price;
    }

    function setSKU($sku){
        $this->sku = $sku;
    }

    function getSKU(){
        return $this->sku;
    }

    function setCategory($category){
        $this->category = $category;
    }

    function getCategory(){
        $db = new DB();
        $conn = $db->db();

        $sql = "SELECT category FROM Products WHERE SKU = ?";
        $stmt = $conn->prepare($sql);
        $stmt->bind_param("s", $this->sku);   
        $stmt->execute();
        $stmt->bind_result($this->category);
        $stmt->fetch();

        $conn->close();
        return $this->category;
    }


    function save(){

        $db = new DB();
        $conn = $db->db();
 
        $sql = "INSERT INTO Products (sku, name, price, category, attribute) VALUES (?, ?, ?, ?, ?)";
                                                
        $stmt = $conn->prepare($sql);
        $stmt->bind_param("sssss", $this->sku, $this->name, $this->price, $this->category, $this->attribute);   
        
        if(!$stmt->execute()){
            throw new Exception($stmt->error);
        }

        $conn->close();
        
    }

    function delete(){
        $sku = $this->getSKU();
        $db = new DB();
        $conn = $db->db();
 
        $sql = "DELETE FROM Products WHERE SKU = ?";
                                                
        $stmt = $conn->prepare($sql);
        $stmt->bind_param("s", $sku);   
        $stmt->execute();

        $conn->close();
    }



    abstract function setAttribute($attribute);
    abstract function getAttribute();
    abstract function jsonSerialize();
    abstract function displayForm();  
}

?>