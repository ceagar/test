<?php
class ProductsWorker{
    private $products = array();

    function __construct(){
        $db = new DB();
        $conn = $db->db();

        $sql = "SELECT SKU, category FROM Products";
        $result = $conn->query($sql);
        while($row = $result->fetch_assoc()){
            $product = $row["category"];
            $objectReflection = new ReflectionClass($product);
            $product = $objectReflection->newInstance();
            $SKU = $row["SKU"];
            $product->setSKU($SKU);
            $product->setCategory($row["category"]);
            $this->products[] = $product;

        } 

        $conn->close();
    }



    function massDelete($array){
        $whatIWant = substr($array, strpos($array, "="));
        $SKUs = explode(" ", $whatIWant);    
        foreach($this->products as $product){
            if(in_array($product->getSKU(), $SKUs)){
                $product->delete();
            }
        }

      
    }

    function getProducts(){
       
        foreach($this->products as $product){
           
            $product = $product->jsonSerialize();
        }

        return json_encode($this->products);
    }
}

?>