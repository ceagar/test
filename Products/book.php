<?php
class Book extends Product {
    
    function setAttribute($weight){
        $this->attribute = "Weight: ".$weight."KG";
    }

    function getAttribute(){
        $sku = $this->getSKU();

        $db = new DB();
        $conn = $db->db();

        $sql = "SELECT attribute FROM Products WHERE SKU = ?";
        $stmt = $conn->prepare($sql);
        $stmt->bind_param("s", $sku);   
        $stmt->execute();
        $stmt->bind_result($this->attribute);
        $stmt->fetch();

        $conn->close();

        return $this->attribute;
    }
  

    function displayForm(){
      return "<div id='specForm'><br><br>
      Weight(kg): <input  id='weight' type='text' name='attribute' value=''>
     <span class='error'>* </span>
     <p>Please provide weight of the book.</p>
     <br><br></div>

     <script>
    function testSpecForm(){
        var weight = document.getElementById('weight').value; 
        var numbers = /^[0-9']+$/;

        var weightError = '';
        if(weight){
            if(!weight.match(numbers)){
                weightError = 'Weight: Please insert numbers only!';
            }
        }
        else{
            weightError = 'Weight is required';
        }

        document.getElementById('error_attribute').innerHTML = weightError;

        if(!weightError){
            formData.attribute = weight;  
            return true;
          }
          else{
            return false;
          }
        
    }
    </script>";
    }

    function jsonSerialize(){
        return [
            'SKU' => $this->getSKU(),
            'name' => $this->getName(),
            'price' => $this->getPrice(),
            'attribute' => $this->getAttribute()
        ];
    }
}

?>