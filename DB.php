<?php
class DB{
   private $servername = "localhost";
   private $username = "root";
   private $password = "";
   private $database = "Products";
   private $conn; 
    
    function __construct(){
        $this->conn = new mysqli($this->servername, $this->username, $this->password, $this->database);
    }
    
    
    function db(){
        if ($this->conn->connect_error) {
          die("Connection failed: " . $this->conn->connect_error);
        } 
        
        return $this->conn;
    }
}
?>