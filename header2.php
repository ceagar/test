<html>
    <head>
        <link href="header.css" rel="stylesheet" type="text/css">
    </head>
    <body>

    <div class="topnav" id="myTopnav">
        <h2>Product Add</h2>
        <div class="buttons">
            <button id="add-product-btn" onClick='save()'>Save</button>
            <button id="delete-product-btn" onClick='cancel()'>Cancel</button>
        </div>
    </div>
    </body>
</html>

<script>
function cancel(){
    window.location = 'index.php';
}
</script>