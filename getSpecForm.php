<?php
  require "Products/product.php";
  require "Products/book.php";
  require "Products/dvd.php";
  require "Products/furniture.php";


  if ($_SERVER['REQUEST_METHOD'] === 'POST') {

   $data = json_decode(file_get_contents('php://input'));
   $objectReflection = new ReflectionClass($data->category);
   $product = $objectReflection->newInstance();
   $product->setSKU($data->sku);
   $product->setName($data->name);
   $product->setPrice($data->price);
   $product->setCategory($data->category);
   $product->setAttribute($data->attribute);

   try{
      $product->save();
   }
   catch(Exception $exp){
      http_response_code(400);
      echo $e->getMessage();
   }
   
}

 if(isset($_GET['category'])){

    $objectReflection = new ReflectionClass($_GET['category']);
    $product = $objectReflection->newInstance();
    echo $product->displayForm();
   
 }

?>