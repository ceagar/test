<?php
    require "Products/product.php";
    require "Products/book.php";
    require "Products/dvd.php";
    require "Products/furniture.php";

    require "Products/ProductsWorker.php";
    
    $worker = new ProductsWorker();
    if(isset($_REQUEST['delete']))
    {
        if(isset($_POST['SKU'])){
            echo $worker->massDelete($_POST['SKU']);
        }
        else
            echo "No products to delete";
    }
    else
        echo $worker->getProducts();
      
   
?>