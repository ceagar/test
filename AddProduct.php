<html>
    <head>
    <link href="AddProduct.css" rel="stylesheet" type="text/css">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    </head>
    <body>
        <?php 
        require 'header2.php';
        require "Products/product.php";
        require "Products/book.php";
        require "Products/dvd.php";
        require "Products/furniture.php"
        ?>

        <p><span class="error">* required field</span></p>
        <p id="error_sku" class="error"></p>
        <p id="error_name" class="error"></p>
        <p id="error_price" class="error"></p>
        <p id="error_category" class="error"></p>
        <p id='error_attribute' class='error'></p>
        <p id='error_query' class='error'></p>


        <form id="product_form" method="post" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>">  
            SKU: <input  id="sku" type="text" name="sku" value="">
            <span class="error">*</span>
            <br><br>
            Name: <input  id="name" type="text" name="name" value="">
            <span class="error">* </span>
            <br><br>
            Price($): <input id="price" type="text" name="price" value="">
            <span class="error">* </span>
            <br><br>
            <label for="productType">Choose a category:</label>
            <select name="category" id="productType" onchange="change(this.value)">
                <option value="">Select Category</option>
                <option id="Book" value="Book">Book</option>
                <option id="DVD" value="DVD">DVD</option>
                <option id="Furniture" value="Furniture">Furniture</option>
            </select>
            <span id="typeError" class="error">* </span>
            <br id="afterSelector">
        </form>

    </body>
</html>
 
<script>

  var formData = {};

function change(category){
    var selectBox = document.getElementById("productType");

    var prev = document.getElementById("specForm");
    if(prev){  
      prev.remove();
    }
    
    if(category != ""){
      xhttp = new XMLHttpRequest();
      xhttp.onreadystatechange = function() {
      if (this.readyState == XMLHttpRequest.DONE && this.status == 200) {

        x = this.responseText.split('<script>');
        document.getElementById("afterSelector").insertAdjacentHTML('afterend', x[0]);
        y = x[1].split("<\/script>");
        script = y[0];
        var newScript = document.createElement("script");
        newScript.innerHTML = script;
        document.getElementById("specForm").appendChild(newScript);
      }
      }
      xhttp.open("GET", "getSpecForm.php?category="+category);
      xhttp.send();
    }
    
}



function testForm(){
  var sku = document.getElementById("sku").value;
  var name = document.getElementById("name").value;
  var price = document.getElementById("price").value;
  var category = document.getElementById("productType").value;

  var skuError = "";
  var nameError = "";
  var priceError = "";
  var categoryError = "";
  

  var lettersNumbers = /^[0-9a-zA-Z]+$/;
  var numbers = /^[0-9]+$/;

  if(sku){

    if(!sku.match(lettersNumbers)){
      skuError = "SKU: Please insert alphanumeric characters only!";
    }
  }
  else{
    skuError = "SKU is required\n";
  }

  if(name){
    if(!name.match(lettersNumbers)){
      nameError = "Name: Please insert alphanumeric characters only!\n" ;
    }
  }
  else{
    nameError = "Name is required\n";
  }

  if(price){
    if(!price.match(numbers)){
      priceError = "Price: Please insert numbers only!\n" ;
    }
  }
  else{
    priceError = "Price is required\n";
  }

  if(!category){
    categoryError = "Category is required\n";
  }

  document.getElementById("error_sku").innerHTML = skuError;
  document.getElementById("error_name").innerHTML = nameError;
  document.getElementById("error_price").innerHTML = priceError;
  document.getElementById("error_category").innerHTML = categoryError;


  if(!skuError && !nameError && !priceError && !categoryError){
    formData.sku = sku;
    formData.name = name;
    formData.price = price;
    formData.category = category;
    return true;
  }
  else{
    return false;
  }

}

  function save(){
    let resultOfTestForm = testForm();
    let resultOfSpecTestForm= testSpecForm();
    if(resultOfTestForm && resultOfSpecTestForm){
      xhttp = new XMLHttpRequest();
      xhttp.onreadystatechange = function() {
        if (this.readyState == XMLHttpRequest.DONE && this.status == 200) {
          window.location = 'index.php';
        }
        if (this.readyState == XMLHttpRequest.DONE && this.status != 200){
        document.getElementById("error_query").innerHTML = "There is an object with this SKU. Try another SKU!";
      }
      }
      xhttp.open("POST", "getSpecForm.php", true);
      xhttp.setRequestHeader("Content-Type", "application/json");
      xhttp.send(JSON.stringify(formData));
    }
    
  }


</script>
