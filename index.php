<html>
    <head>
    <link href="index.css" rel="stylesheet" type="text/css">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    </head>
    <body>
        <?php require 'header.php';?>
        <div id = "row" class = "row">
        </div>
        <script>
            xmlhttp = new XMLHttpRequest();
            xmlhttp.onload = function(){
              products = JSON.parse(this.responseText);
              for(let i = 0; i <  products.length; i++ ){
                
                var card = `
                <div class ="card">
                    <input class="delete-checkbox" type = "checkbox"'>
                    <div class = "container">
                        <div id = "SKU">${products[i]['SKU']}</div>
                        <div id = "name">${products[i]['name']}</div>
                        <div id = "price">${products[i]['price']}</div>
                        <div id = "attribute">${products[i]['attribute']}</div>
                    </div>
                </div>`;
                var element = document.createElement('div');
                element.className = "column";
                element.innerHTML= card;
                document.getElementById("row").appendChild(element);  
              }
              
            }
            xmlhttp.open("GET", "ProductsClient.php");
            xmlhttp.send();


            function massDelete(){
                 documents = document.getElementsByClassName("card");
                 arr='SKU=';
                for(let i = 0; i < documents.length; i++){
                    if(documents[i].firstElementChild.checked == true){
                        arr += documents[i].lastElementChild.firstElementChild.innerHTML + " ";
                    }
                }
                console.log(arr);
                xmlhttp = new XMLHttpRequest();
                xmlhttp.onreadystatechange = function() {
                    if( xmlhttp.readyState==XMLHttpRequest.DONE && xmlhttp.status==200 ){
                         document.location.reload();
                    }
                };

                xmlhttp.open("POST", "ProductsClient.php?delete=true", false);
                xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
                xmlhttp.send(arr);


            }

            function addProduct(){
                window.location = 'AddProduct.php' ;
            }
        </script>
       
    </body>
</html>
